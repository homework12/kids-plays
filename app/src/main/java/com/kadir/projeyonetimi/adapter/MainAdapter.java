package com.kadir.projeyonetimi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kadir.projeyonetimi.R;
import com.kadir.projeyonetimi.model.MainGame;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {




    Context context;
    ArrayList<MainGame> itemList ;



    public MainAdapter(Context context, ArrayList<MainGame> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_main_menu, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MainGame mainGame=itemList.get(position);

        holder.txtGameName.setText(mainGame.getName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(mainGame.getIntent());
            }
        });
        Glide.with(context).load(mainGame.getDrawable()).into(holder.imgCat);

    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtGameName;
        CardView cardView;
        ImageView imgCat;

        ViewHolder(View itemView) {
            super(itemView);
            txtGameName = itemView.findViewById(R.id.txtGameName);
            cardView = itemView.findViewById(R.id.constRvItem);
            imgCat = itemView.findViewById(R.id.imgCat);

        }
    }
}


