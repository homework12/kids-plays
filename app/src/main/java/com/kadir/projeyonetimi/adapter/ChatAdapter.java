package com.kadir.projeyonetimi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.kadir.projeyonetimi.R;
import com.kadir.projeyonetimi.model.Chat;
import com.kadir.projeyonetimi.model.MainGame;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {




    Context context;
    ArrayList<Chat> itemList ;



    public ChatAdapter(Context context, ArrayList<Chat> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_chat, parent, false);
        return new ChatAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ChatAdapter.ViewHolder holder, int position) {
        final Chat chat=itemList.get(position);

        if(chat.isRobot()){
            holder.cardViewRobot.setVisibility(View.VISIBLE);
            holder.cardViewHuman.setVisibility(View.GONE);
            holder.txtRobot.setText(chat.getMessage());
        }else{
            holder.cardViewHuman.setVisibility(View.VISIBLE);
            holder.cardViewRobot.setVisibility(View.GONE);
            holder.txtHuman.setText(chat.getMessage());
        }


    }

    public void updateData(ArrayList<Chat> viewModels) {

        itemList.addAll(viewModels);
        notifyDataSetChanged();
    }
    public void setItems(ArrayList<Chat> chats) {
        this.itemList = itemList;
    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardViewRobot,cardViewHuman;
        TextView txtHuman,txtRobot;

        ViewHolder(View itemView) {
            super(itemView);
            cardViewRobot=itemView.findViewById(R.id.cardRobot);
            cardViewHuman=itemView.findViewById(R.id.cardViewHuman);
            txtHuman=itemView.findViewById(R.id.txtHuman);
            txtRobot=itemView.findViewById(R.id.txtRobot);

        }
    }
}