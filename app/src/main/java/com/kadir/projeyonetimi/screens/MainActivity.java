package com.kadir.projeyonetimi.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.kadir.projeyonetimi.R;
import com.kadir.projeyonetimi.adapter.MainAdapter;
import com.kadir.projeyonetimi.model.MainGame;
import com.kadir.projeyonetimi.screens.games.AlienLanguageActivity;
import com.kadir.projeyonetimi.screens.games.ChameleonActivity;
import com.kadir.projeyonetimi.screens.games.ChatRobotActivity;
import com.kadir.projeyonetimi.screens.games.JournerToSchoolActivity;
import com.kadir.projeyonetimi.screens.games.JudgeBook;
import com.kadir.projeyonetimi.screens.games.MakeMeHappyActivity;
import com.kadir.projeyonetimi.screens.games.SchoolLibraryActivity;
import com.kadir.projeyonetimi.screens.games.SortingHatActivity;
import com.kadir.projeyonetimi.screens.games.TitanicActivity;
import com.kadir.projeyonetimi.screens.games.TouristInfoActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


    }



    private void init() {
        RecyclerView rvGames=findViewById(R.id.rvGames);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        rvGames.setLayoutManager(gridLayoutManager);
        MainAdapter gamesAdapter = new MainAdapter(getApplicationContext(),getGamesData());
        rvGames.setAdapter(gamesAdapter);
    }
    private ArrayList<MainGame> getGamesData() {
        ArrayList<MainGame> itemListGames=new ArrayList<>();

        MainGame game;
        game=new MainGame();
        game.setName("Chat Robot");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_robot));
        game.setIntent(new Intent(getApplicationContext(), ChatRobotActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Judge a Book ");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_book));
        game.setIntent(new Intent(getApplicationContext(), JudgeBook.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Chameleon");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_paint));
        game.setIntent(new Intent(getApplicationContext(), ChameleonActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Alien Language");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_spaceship));
        game.setIntent(new Intent(getApplicationContext(), AlienLanguageActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Journey to Scholl");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_bus));
        game.setIntent(new Intent(getApplicationContext(), JournerToSchoolActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("School Library");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_read));
        game.setIntent(new Intent(getApplicationContext(), SchoolLibraryActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Sorting Hat");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_witch));
        game.setIntent(new Intent(getApplicationContext(), SortingHatActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Titanic");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_titanic));
        game.setIntent(new Intent(getApplicationContext(), TitanicActivity.class));
        itemListGames.add(game);

        game=new MainGame();
        game.setName("Tourist Info");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_tourism));
        game.setIntent(new Intent(getApplicationContext(), TouristInfoActivity.class));
        itemListGames.add(game);

         game=new MainGame();
        game.setName("Make me Happy");
        game.setDrawable(getResources().getDrawable(R.drawable.ic_feelings));
        game.setIntent(new Intent(getApplicationContext(), MakeMeHappyActivity.class));
        itemListGames.add(game);
        return itemListGames;
    }


}
