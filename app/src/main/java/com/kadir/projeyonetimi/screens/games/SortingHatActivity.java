package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kadir.projeyonetimi.R;

import java.util.Hashtable;
import java.util.Random;

public class SortingHatActivity extends AppCompatActivity {

    Hashtable<Integer, String> hashTable;
    String[] mekanlar=new String[]{"Çarşıya gidebilirsin","Kırtasiyeye gidebilirsin","Geziye gidebilirsin","Okula gidebilirsin","Pastaneye gidebilirsin"};
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_secmensapka_activity);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.hashTable = new Hashtable<>();
    }

    public void btnGetStatus(View view) {
        setTrainData();
        TextView txtSonuc = (TextView) findViewById(R.id.txtResult);
        String val=((EditText) findViewById(R.id.edtName)).getText().toString();
        EditText editText=findViewById(R.id.edtName);
        String message=editText.getText().toString();
        if(message!=null && !message.isEmpty() ){
            try {
                txtSonuc.setText(hashTable.get(val.length()%10));
            }
            catch (Exception ex){


            }
        }else{
            Toast.makeText(this, "Boş alanı doldurunuz lütfen", Toast.LENGTH_SHORT).show();
        }






    }

    public void setTrainData() {
        hashTable.put(0,"Büyücülük Okuluna Gideceksin!");
        hashTable.put(1,"Aşçılık Okuluna Gideceksin");
        hashTable.put(2,"Atçılık Okuluna Gideceksin");
        hashTable.put(3,"Cadılar Okuluna Gideceksin");
        hashTable.put(4,"Periler Okuluna Gideceksin");
        hashTable.put(6,"Faniler Okuluna Gideceksin");
        hashTable.put(7,"Şeyatanlar Okuluna Gideceksin");
        hashTable.put(8,"Ahmaklar Okuluna Gideceksin");
        hashTable.put(9,"Harry Okuluna Gideceksin");




    }
}
