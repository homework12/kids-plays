package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kadir.projeyonetimi.R;
import com.kadir.projeyonetimi.model.Ogrenci;

import java.util.ArrayList;
import java.util.Hashtable;

public class JournerToSchoolActivity extends AppCompatActivity {


    Hashtable<Integer,String> hashTable;
    String durum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okuluyolu_activity);
        hashTable=new Hashtable<Integer,String>();
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void btnGetStatus(View view) {
        setTrainData();
        
        EditText edtYas=(EditText)findViewById(R.id.edtYas);
        EditText edtArkadas=(EditText)findViewById(R.id.edtArkadas);
        EditText edtMesafe=(EditText)findViewById(R.id.edtMesafe);
        TextView txtSonuc=(TextView) findViewById(R.id.txtResult);
        
        String message=edtYas.getText().toString();
        String message1=edtArkadas.getText().toString();
        String message2=edtMesafe.getText().toString();
        if(message!=null && !message.isEmpty() && message1!=null && !message1.isEmpty() && message2!=null && !message2.isEmpty()){
            float valueTotal= Math.round(Float.parseFloat(edtYas.getText().toString())*10+
                    Float.parseFloat(edtMesafe.getText().toString())*10+
                    Float.parseFloat(edtArkadas.getText().toString())*4);
            Log.e("", String.valueOf(valueTotal));
            valueTotal=valueTotal%15;
            Log.e("", String.valueOf(valueTotal));

            String result="";

            result=  hashTable.get((int)valueTotal);
            txtSonuc.setText("Okula "+result+ " gidiyorsun..");
        }else {
            Toast.makeText(this, "Lütfen Boş alanları doldurunuz", Toast.LENGTH_SHORT).show();
        }



      




    }

    void setTrainData() {

        ArrayList<Ogrenci> Ogrenciler=new ArrayList<>();
        Ogrenciler.add(new Ogrenci(10, (float) 1.22,5));

        Ogrenciler.add(new Ogrenci(8, (float) 1.33,3));

        Ogrenciler.add(new Ogrenci(7, (float) 2,1));

        Ogrenciler.add(new Ogrenci(5, (float) 3,8));
        Ogrenciler.add(new Ogrenci(4, (float) 3.5,4));
        Ogrenciler.add(new Ogrenci(9, (float) 1.5,2));
        Ogrenciler.add(new Ogrenci(4, (float) 1.7,1));
        Ogrenciler.add(new Ogrenci(5, (float) 0.22,3));
        Ogrenciler.add(new Ogrenci(6, (float) 0.66,3));
        Ogrenciler.add(new Ogrenci(14, (float) 1.13,4));
        Ogrenciler.add(new Ogrenci(16, (float) 1.17,6));
        Ogrenciler.add(new Ogrenci(6, (float) 1.25,5));
        Ogrenciler.add(new Ogrenci(13, (float) 1.18,4));
        Ogrenciler.add(new Ogrenci(12, (float) 1.13,1));
        Ogrenciler.add(new Ogrenci(4, (float) 1.11,8));
        Ogrenciler.add(new Ogrenci(7, (float) 1.7,3));
        Ogrenciler.add(new Ogrenci(6, (float) 1.9,2));
        Ogrenciler.add(new Ogrenci(4, (float) 2.15,1));




        int i=0;
        for(Ogrenci in:Ogrenciler)
        {
            int toplamPuan=(int)(in.ArkadasSayisi*10+in.Mesafe*10+in.Yas*10);
            toplamPuan=toplamPuan%3;
             durum="";
            switch (toplamPuan)
            {
                case 0:
                    durum="Arabayla";
                    break;
                case 1:
                    durum="Yürüyerek";
                    break;
                case 2:
                    durum="Bisikletle";
                    break;

                default:
                    durum="Gitmiyor";
                    break;
            }

            hashTable.put(i,durum);
            i++;
            Log.e("",i  +  durum);
        }







    }


}
