package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kadir.projeyonetimi.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

public class TouristInfoActivity extends AppCompatActivity {


    Hashtable<String, String> hashTable;
   String[] mekanlar=new String[]{"Çarşıya gidebilirsin","Kırtasiyeye gidebilirsin","Geziye gidebilirsin","Okula gidebilirsin","Pastaneye gidebilirsin"};
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_turist_activity);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.hashTable = new Hashtable<>();
    }

    public void btnGetStatus(View view) {
        setTrainData();
        TextView txtSonuc = (TextView) findViewById(R.id.txtResult);
        String ilgiAlani=((EditText) findViewById(R.id.edtName)).getText().toString();

        EditText editText=findViewById(R.id.edtName);
        String message=editText.getText().toString();
        if(message!=null && !message.isEmpty()){
            ilgiAlani=ilgiAlani.toLowerCase();

            try {
                Log.e("",ilgiAlani);
                txtSonuc.setText(hashTable.get(ilgiAlani).toString());
            }
            catch (Exception ex){
                Random random=new Random();

                int val=random.nextInt(5);
                txtSonuc.setText(mekanlar[val]);
                Log.e("",ilgiAlani);

            }
        }else{
            Toast.makeText(this, "Lütfen boş alanı doldurunuz", Toast.LENGTH_SHORT).show();
        }





    }

    public void setTrainData() {
        ArrayList<String> ilgiAlanlari=new ArrayList<>();

        ilgiAlanlari.add("spor");
        ilgiAlanlari.add("tarih");
        ilgiAlanlari.add("siyaset");
        ilgiAlanlari.add("müzik");
        ilgiAlanlari.add("oyun");
        ilgiAlanlari.add("teknoloji");


        hashTable.put("spor","Stadyuma gidebilirsin");
        hashTable.put("tarih","Müzeye gidebilirsin");
        hashTable.put("siyaset","Meclise gidebilirsin");
        hashTable.put("müzik","Operaya gidebilirsin");
        hashTable.put("oyun","Oyun merkezine gidebilirsin");
        hashTable.put("teknoloji","Tesla müzesine gidebilirsin");


    }
}
