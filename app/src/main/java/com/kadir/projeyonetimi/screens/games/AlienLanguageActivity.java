package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.kadir.projeyonetimi.R;

import java.util.Locale;

public class AlienLanguageActivity extends AppCompatActivity {
    TextToSpeech t1;
    ImageView gif;
    EditText edtWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alien_activity);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
            gif= findViewById(R.id.imgGif);

                String imageUrl ="https://media.tenor.com/images/6561f7c44e9447606c73acd840400286/tenor.gif" ;
                Glide.with(getApplicationContext())
                .load(imageUrl)
                .into(gif);


            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                t1.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {



                    }

                    @Override
                    public void onDone(String utteranceId) {

                    }

                    @Override
                    public void onError(String utteranceId) {


                    }
                });
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(new Locale("tr", "TR"));

                }

            }

        });


    }

    public void btnSpeak(View view) {

        edtWord=(EditText)findViewById(R.id.edtSpeech);
        String message=edtWord.getText().toString();
        if(message!=null && !message.isEmpty()){
            t1.speak(message, TextToSpeech.QUEUE_FLUSH, null);
        }else{
            t1.speak("Bir şeyler yazıp konuşmamı sağlar mısın?", TextToSpeech.QUEUE_FLUSH, null);
        }

    }

}
