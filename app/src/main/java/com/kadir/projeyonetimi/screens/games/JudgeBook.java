package com.kadir.projeyonetimi.screens.games;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kadir.projeyonetimi.R;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class JudgeBook extends AppCompatActivity {

    static final int READ_WRITE_STORAGE = 52;
    static final int PICK_REQUEST = 53;
    RoundedImageView imgBook;
    Button btnSelectBook;
    TextView txtBookComment;
    ArrayList<String> itemList=new ArrayList<>();
    Bitmap bitmap=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_judge_book);
        init();
        btnSelectBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , PICK_REQUEST);
                }
            }
        });
        initComment();



    }

    private void initComment() {
        itemList.add("Harika Seçim");
        itemList.add("Harika Seçim1");
        itemList.add("Harika Seçim2");
        itemList.add("Harika Seçim3");
    }

    private void init() {
        btnSelectBook=findViewById(R.id.btnSelectBook);
        txtBookComment=findViewById(R.id.txtBookComment);
        imgBook=findViewById(R.id.imgBook);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_REQUEST:
                    try {
                        Uri uri = data.getData();
                         bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        imgBook.setImageBitmap(getResizedBitmap(bitmap,800,800));
                        Random r=new Random();
                        txtBookComment.setText(itemList.get(r.nextInt(itemList.size())));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
    private boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{permission},
                    READ_WRITE_STORAGE);
        }
        return isGranted;
    }
    private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();

        matrix.postScale(scaleWidth, scaleHeight);


        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return  resizedBitmap;

    }





}
