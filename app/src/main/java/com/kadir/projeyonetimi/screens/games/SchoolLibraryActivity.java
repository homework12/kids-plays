package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kadir.projeyonetimi.R;

import java.util.Hashtable;
import java.util.Random;

public class SchoolLibraryActivity extends AppCompatActivity {

    Hashtable<Integer, String[]> hashTable;
    String[] mekanlar=new String[]{"Çarşıya gidebilirsin","Kırtasiyeye gidebilirsin","Geziye gidebilirsin","Okula gidebilirsin","Pastaneye gidebilirsin"};
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_okul_kutuphanesi);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.hashTable = new Hashtable<>();
    }

    public void btnGetStatus(View view) {
        setTrainData();
        TextView txtSonuc = (TextView) findViewById(R.id.txtResult);
        String val=((EditText) findViewById(R.id.edtName)).getText().toString();
        EditText editText=findViewById(R.id.edtName);
        String message=editText.getText().toString();
        if(message!=null && !message.isEmpty() ){
            String[] arrSuggest=hashTable.get(Math.round(Float.parseFloat(val)));


            Random random=new Random();
            try {
                txtSonuc.setText("Sana önerdiğim kitap: "+arrSuggest[random.nextInt(5)]);
            }
            catch (Exception ex){


            }
        }else{
            Toast.makeText(this, "Boş alanı doldurunuz lütfen", Toast.LENGTH_SHORT).show();
        }




    }

    public void setTrainData() {

        String [] arrBirAlti=new  String[]{"Araba Sevdası","Telemak","Gulyabani"," Domaniç Dağlarının Yolcusu"};
        String [] arrBirIki=new  String[]{" Damla Damla","Havaya Uçan At","Koçyiğit Köroğlu","87 Oğuz"};
        String [] arrBirUc=new  String[]{"Bomba","Bitmeyen Gece","Tiryaki Sözleri ","Osmancık "};
        String [] arrBirDort=new  String[]{"Robin Hood","Pinokyo ","Uçan Sınıf","Şamatalı Köy"};
        String [] arrBirBes=new  String[]{"İnsan Ne İle Yaşar","Pollyanna ","Heidi","Seksen Günde Devr-i Âlem "};

        hashTable.put(0,arrBirAlti);
        hashTable.put(1,arrBirIki);
        hashTable.put(2,arrBirUc);
        hashTable.put(3,arrBirDort);
        hashTable.put(4,arrBirBes);


    }
}
