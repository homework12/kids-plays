package com.kadir.projeyonetimi.screens.games;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kadir.projeyonetimi.R;

import java.util.Random;

public class MakeMeHappyActivity extends AppCompatActivity {

    String tell;
    EditText edtTell;
    Button btnTell;
    ImageView imgTell;
    Drawable[] emojiID =new Drawable[4];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_me_happy);
        init();
        btnTell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               tell=edtTell.getText().toString();
                if(tell!=null && !tell.isEmpty()){
                    Random random=new Random();
                    int a=random.nextInt(4);
                    Log.e("result",a+"");
                    imgTell.setImageDrawable(emojiID[a]);
                }else{
                    Toast.makeText(MakeMeHappyActivity.this, "Lütfen bana bişeyler söyle", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void init() {
        btnTell=findViewById(R.id.btnTell);
        edtTell=findViewById(R.id.edtTell);
        imgTell=findViewById(R.id.imgEmoji);

        emojiID[0]=getResources().getDrawable(R.drawable.ic_sad);
        emojiID[1]=getResources().getDrawable(R.drawable.ic_smile);
        emojiID[2]=getResources().getDrawable(R.drawable.ic_smile_ters);
        emojiID[3]=getResources().getDrawable(R.drawable.ic_surprised);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
