package com.kadir.projeyonetimi.screens.games;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kadir.projeyonetimi.R;

import java.util.Hashtable;

public class TitanicActivity extends AppCompatActivity {
    Hashtable<Integer, Boolean> hashTable;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_titanic_activity);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.hashTable = new Hashtable<>();
    }

    public void btnGetStatus(View view) {
        setTrainData();
        TextView txtSonuc = (TextView) findViewById(R.id.txtResult);
        int isimUzunlugu = ((EditText) findViewById(R.id.edtName)).getText().length();

        EditText editText=findViewById(R.id.edtName);
        String message=editText.getText().toString();
        if(message!=null && !message.isEmpty()){
            if (isimUzunlugu <= 2) {
                return;
            }
            if (((Boolean) this.hashTable.get(Integer.valueOf(isimUzunlugu))).booleanValue()) {
                txtSonuc.setText("Tebrikler Yaşıyorsun!");
            } else {
                txtSonuc.setText("Üzgünüm Kurtulamadın!");
            }
        }else{
            Toast.makeText(this, "Lütfen boş alanı doldurunuz", Toast.LENGTH_SHORT).show();
        }


    }

    /* access modifiers changed from: 0000 */
    public void setTrainData() {
        String[] persons = {"Mrs Caroline LaneMajor Archibald Willingham", "Mr Alexander Milne (Manservant to Mr William Ernest Carter)", "Mr Edward Pennington", "Mrs Helen Churchill", "Mrs Charlotte Wardle", "Mrs Lucile", "Miss Lucile Polk", "Mrs Edith Martha Bowerman", "Mr Howard Brown", "Mrs Eleanor Genevieve", "Miss Gladys", "Mr Tyrell William", "Mrs Virginia Estelle", "Mrs Malvina Helen", "Captain Edward Gifford"};
        for (int i = 0; i < 15; i++) {
            if (persons[i].length() % 2 == 0) {
                this.hashTable.put(Integer.valueOf(i), Boolean.valueOf(false));
            } else {
                this.hashTable.put(Integer.valueOf(i), Boolean.valueOf(true));
            }
        }
    }

}
