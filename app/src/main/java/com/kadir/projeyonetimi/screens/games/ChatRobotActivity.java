package com.kadir.projeyonetimi.screens.games;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kadir.projeyonetimi.R;
import com.kadir.projeyonetimi.adapter.ChatAdapter;
import com.kadir.projeyonetimi.adapter.MainAdapter;
import com.kadir.projeyonetimi.model.Chat;
import com.kadir.projeyonetimi.model.MainGame;

import java.util.ArrayList;
import java.util.Random;

public class ChatRobotActivity extends AppCompatActivity implements View.OnClickListener{

    EditText edtMessage;
    ArrayList<Chat> itemList=new ArrayList<>();
    ArrayList<String> messageRobot=new ArrayList<>();
    ChatAdapter chatAdapter;
    int sayac=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_robot);
        init();
        initRobotData();
    }




    private void init() {
        edtMessage=findViewById(R.id.edtMessage);
        findViewById(R.id.imgMessageSend).setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RecyclerView rvChat=findViewById(R.id.rv_chat);
        rvChat.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        chatAdapter = new ChatAdapter(getApplicationContext(),getChatData());
        rvChat.setAdapter(chatAdapter);
    }

    private ArrayList<Chat> getChatData() {

        Chat chat=new Chat();
        chat.setRobot(true);
        chat.setMessage("Merhaba bugün nasılsınız?");
        itemList.add(chat);


        return itemList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgMessageSend:
                sendChat();
                break;
        }
    }

    private void sendChat() {

        Chat chatHuman=new Chat();
        String message=edtMessage.getText().toString();
        if(message!=null && !message.isEmpty() && message.length()>4){
            chatHuman.setMessage(message);
            chatHuman.setRobot(false);
            itemList.add(chatHuman);
            Chat chatRobot=new Chat();
            chatRobot.setMessage(getRobotMessage());
            chatRobot.setRobot(true);
            itemList.add(chatRobot);
            chatAdapter.setItems(itemList);
            chatAdapter.notifyDataSetChanged();
        }else {
            Chat chatRobot=new Chat();
            chatRobot.setMessage("Lütfen anlamlı bir şey yaz :)");
            chatRobot.setRobot(true);
            itemList.add(chatRobot);
            chatAdapter.setItems(itemList);
            chatAdapter.notifyDataSetChanged();
        }

    }
    private void initRobotData() {
        messageRobot.add("Bende İyiyim günün nasıl geçiyor?");
        messageRobot.add("Teşekkür ederim sizi mutlu etmeye çalışıyorum");
        messageRobot.add("Kendimi pek iyi hissetmiyorum.");

        messageRobot.add("Anladım senin için ne yapabilirim?");
        messageRobot.add("İyi düşünmüssün bu konuda sana yardımcı olabilir miyim?");
        messageRobot.add("Muhteşem!! Bende sana yardım edebilir miyim?");

        messageRobot.add("Tabi sana yardımcı olabilirim.");
        messageRobot.add("Hayır üzgünüm bunu gerçekleştiremiyeceğim.");
        messageRobot.add("Bunu düşünmem gerek.");



        messageRobot.add("Sanırım ayarlarım Bozuldu...Dediğini tam olarak anlayamadım daha sonra tekrar dener misin?");





    }

    private String getRobotMessage(){

        Random random=new Random();
        int rand=random.nextInt(sayac+3-sayac) + sayac;
        Log.e("rand",rand+"");
        if(rand>9){
            return messageRobot.get(9);
        }
        String message=messageRobot.get(rand);

        sayac=sayac+3;
        return message;
    }
}
