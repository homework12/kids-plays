package com.kadir.projeyonetimi.model;

public class Chat {

    boolean robot;
    String Message;

    public Chat() {
    }

    public boolean isRobot() {
        return robot;
    }

    public void setRobot(boolean robot) {
        this.robot = robot;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
