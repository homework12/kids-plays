package com.kadir.projeyonetimi.model;

public class Ogrenci {

    public Ogrenci(float _Yas, float _Mesafe, float _ArkadasSayisi)
    {
        this.Yas=_Yas;
        this.Mesafe=_Mesafe;
        this.ArkadasSayisi=_ArkadasSayisi;

    }

    public float Yas;
    public float Mesafe;
    public float ArkadasSayisi;

}
