package com.kadir.projeyonetimi.model;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;

public class MainGame {

    private String name;
    Intent  intent;

    Drawable drawable;



    public MainGame() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }


    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
