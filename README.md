# Kids Play
Bu Proje Yazılım Proje Yönetimi dersi için geliştirilmiştir.

## Hakkında

Kids Plays küçük yaştaki kişilere makine öğrenmesi mantığının nasıl çalıştığını öğretmeye çalışan bir uygulamadır. Şu anlık Android için geliştirilmiştir. Uygulama geliştirme dili Java'dır.



### Ekran Görüntüleri

![WhatsApp Image 2020-06-07 at 9 15 46 PM](https://user-images.githubusercontent.com/9121424/83980029-1503c080-a91b-11ea-8f69-e4b1a0a265ba.jpeg)
![WhatsApp Image 2020-06-07 at 9 15 26 PM](https://user-images.githubusercontent.com/9121424/83980030-1634ed80-a91b-11ea-9f07-68b1fc16d943.jpeg)
![WhatsApp Image 2020-06-07 at 9 17 31 PM](https://user-images.githubusercontent.com/9121424/83980033-16cd8400-a91b-11ea-8d63-2060dbff113a.jpeg)
![WhatsApp Image 2020-06-07 at 9 16 17 PM](https://user-images.githubusercontent.com/9121424/83980034-16cd8400-a91b-11ea-9205-25d8d399966d.jpeg)